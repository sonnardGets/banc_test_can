# -*- coding: utf-8 -*-
'''
Filename: c:\pythonNS_library\google_json.py
Path: c:\pythonNS_library
Created Date: Tuesday, March 10th 2020, 5:47:40 pm
Author: Nicolas.Sonnard

Copyright (c) 2020 Your Company
'''
import json
import requests
from log import *
import asyncio
from barcode import*


class google_tools(object):
    """classe google elle contiendra les demandes au serveur google ex: test, path repertoir, evetuel stockage.
    
    Arguments:
        object {[type]} -- [description]
    """

    def __init__(self):
        self.url="https://us-central1-fabrication-8bd49.cloudfunctions.net/getProductTestDataV2?produit="
        self.token="&token=dkwuekzqenvloezvbxfhg"
        self.barcode=barcode_reader()

    def get_6chfr(self,bv):

        bv=self.barcode.number_barcode(bv)

        URLInfos="https://www.gets-support.ch/dkeuegksowiwmndjkzsh/dkeugcjfiezwkoezwhxksnaheuf.php?barcode="+str(bv)

     
        getresult = requests.get(URLInfos)
        print(bv)
        # some JSON:
        x=getresult.text

        # test si le numero existe dans la base de données
        if "NAK" in x:  
            self.url="https://us-central1-fabrication-8bd49.cloudfunctions.net/getProductFamilyData?token=dl2ud543fdfjkvbi46sdsyvxkvb%C3%A9lji06ufbcmsfhg&produit="+str(bv)
            article_web =  requests.get(self.url)
            article_web_txt=  article_web.text
            if not "NAK" in article_web_txt:
                return bv

        y = json.loads(x)
        if  "noarticle" in y:
            no6chfr=y.get("noarticle")
            print (no6chfr)

            return no6chfr
        
        return "NAK"


    def get_json_test(self,no_article):
       

       
        #self.url=str(self.no_article)+self.token
        self.log=log()

        url="https://us-central1-fabrication-8bd49.cloudfunctions.net/getProductTestDataV2?produit="+no_article+"&token=dkwuekzqenvloezvbxfhg"

        #url=str(self.url+no_article+self.token)
        article_web =  requests.get(url)
        article_web_txt=  article_web.text

        #debug 
        print ("no6chr -->       "+no_article)
        print ("url request -->  "+str(url))
        print ("article web -->  "+str(article_web))

        try:
            json_articles=json.loads(article_web_txt)
            return json_articles

        except:
            self.log.logging("Error parsing \t\t"+article_web_txt,"google_json.py")
            return "NAK"
            
    def get_json_carton(self,no_bv,initals):

        #URL INFOS CARTON  https://us-central1-fabrication-8bd49.cloudfunctions.net/getProductBoxSerialNumber?quantity=5&bvnumber=231762&token=dl2ud5ddkezbv547ffg23jfr73gdfkgn438hlzi463rfdcmsfhg&user=steph

        self.no_bv=str(no_bv)
        self.url="https://us-central1-fabrication-8bd49.cloudfunctions.net/getProductBoxSerialNumber?quantity=5&bvnumber="+str(no_bv)+"&token=dl2ud5ddkezbv547ffg23jfr73gdfkgn438hlzi463rfdcmsfhg&user="+str(initals)

        self.log=log()
        try:
            article_web =  requests.get(self.url)
            article_web_txt=  article_web.text

            return article_web_txt

        except:
            self.log.logging("Error requests \t\t"+article_web_txt,"google_json.py")
            print ("Error requests \t\t"+article_web_txt)
            return "NAK"


    # permet d'obtenir un json d'informations du produit
    # si un bv est scanné le numéro à 6 chiffre sera extrait.

    def get_json_family_infos(self,numero):
        self.url="https://us-central1-fabrication-8bd49.cloudfunctions.net/getProductFamilyDataV210119?token=dl2ud543fdfjkvbi46sdsyvxkvbélji06ufbcmsfhg&produit="+str(self.get_6chfr(numero))

        self.log=log()
        family_infos =  requests.get(self.url)
        family_infos_txt=  family_infos.text
        try:
            json_articles=json.loads(family_infos_txt)
            return json_articles

        except:
            self.log.logging("Error parsing \t\t"+family_infos_txt,"google_json.py")
            return "NAK"

        
    # retourn la plage de numero de serie afin de verifier l'étiquette colle
    #ex {"productNumber":"104718","familyID":"trEiquqSZ83OSKduZRxj","familyName":"Répéteurs radio","increment":"1","minSerialNumber":553670655,"maxValue":554696703}
    def return_product_range(self):

        self.url="https://us-central1-fabrication-8bd49.cloudfunctions.net/getProductFamilyDataV210120?token=dl2ud543fdfjkvbi46sdsyvxkvb%C3%A9lji06ufbcmsfhg&produit="+str(self.get_6chfr(numero))

        self.log=log()
        family_infos =  requests.get(self.url)
        family_infos_txt=  family_infos.text
        try:
            json_articles=json.loads(family_infos_txt)
            return [json_articles['minSerialNumber'],json_articles['maxValue']];

        except:
            self.log.logging("Error parsing \t\t"+family_infos_txt,"google_json.py")
            return "NAK"

    
    """permet de demander a google les chemin des softs
        
        Arguments:
            object {[entrant no_article ou bv]} -- [renvoie le json brut]
        """

    def return_path_json(self, numero):


        
        self.url="https://us-central1-fabrication-8bd49.cloudfunctions.net/getProductFamilyDataV210119?token=dl2ud543fdfjkvbi46sdsyvxkvbélji06ufbcmsfhg&produit="+str(self.get_6chfr(numero))




        self.log=log()
        family_infos =  requests.get(self.url)
        family_infos_txt=  family_infos.text
        try:
            json_articles=json.loads(family_infos_txt)
            return json_articles

        except:
            self.log.logging("Error parsing \t\t"+family_infos_txt,"google_json.py")
            return "NAK"