import serial
import serial.tools.list_ports
from subprocess import call
from queue import Queue
import time
import json

"""event python seriel
"""

class controller(object):
    """[classe du port seriel, cette classe recoit et envoie les datas sur le port serie via 2 queue cette classe sera appelée en thread afin d'écouter le porte serie
    ]

    """


    def __init__(self):
    
        
        self.ComPort = 'com7'
        self.baud = 115200
        self.thread1 = None
        self.thread2 = None
        self.thread3 =  None
        self.run=True
        print ("start thread serial")


        
    
    def Connect_BT(self):
        


        self.arduino_ports = [
            p.device
            for p in serial.tools.list_ports.comports()
                if 'Arduino' in p.description  # may need tweaking to match new arduinos
                        ]
        if not self.arduino_ports:
            
    
            return
        if len(self.arduino_ports) > 1:
            
            return
    

        try:
            self.ser = serial.Serial(self.arduino_ports[0], self.baud, timeout=None)
        except: 
            print ("error connect")






    def serial(self,rx_datas_queue,tx_datas,queue_gets_tools):
        self.rx_datas=rx_datas_queue
        self.tx_datas=tx_datas
        self.queue_gets_tools=queue_gets_tools
        data = ""
        tx_str=""
        while (self.run==True):
            '''
            if not self.tx_datas.empty():
                print("Envoie data au banc de test")
                tx_datas=self.tx_datas.get(1)
                tx_datas=tx_datas+"\n"
                print(tx_datas)
                self.ser.write(tx_datas.encode())   
            '''
            try:

                while self.ser.in_waiting:



                    data = ""
                    un_data=0
                    while un_data != "\n":
                        if self.ser.inWaiting():
                            un_data = self.ser.read()
                            try:
                                un_data=un_data.decode("utf-8")
                                data+=str(un_data)
                            except:
                                print("erreur serial decoding")

                    if len(data) > 5 and "{" in data[0]:
                        self.queue_gets_tools.put(data, 1)  # Ecrit 'data' dans la file


                    # on envoie des dattas que quand le banc de test envoie un heartbeat        
                    if "?" in data:
                        
                        if not self.tx_datas.empty():
                            print("Envoie data au banc de test")
                            tx_str=self.tx_datas.get(1)
                            tx_str = tx_str.replace("\'", "\"")
                            tx_datas=tx_str
                            tx_datas=tx_datas+"\n"
                            print(tx_datas)
                            self.ser.write(tx_datas.encode())   


                              
                    if connected_bt != True:
                        connected_bt=True
                        json_state_serial={}
                        json_state_serial ["typemsg"]="GUI"
                        json_state_serial ["bt_statut"]="Connecté"
                        queue_gets_tools.put(json.dumps(json_state_serial))
                        

    
            except:
                connected_bt=False
                print ("connecting to bt wait 5 sec ...")
                json_state_serial={}
                json_state_serial ["typemsg"]="GUI"
                json_state_serial ["bt_statut"]="Déconnecté"
                queue_gets_tools.put(json.dumps(json_state_serial))
                self.Connect_BT()
                time.sleep(5)
                

            

 
    
