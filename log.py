'''
Filename: c:\pythonNS_library\log.py
Path: c:\pythonNS_library
Created Date: Tuesday, March 10th 2020, 6:01:10 pm
Author: Nicolas.Sonnard

Copyright (c) 2020 Your Company

'''
import os.path
from datetime import datetime

class log(object):
    """cette class va créer automatiquement un dossier log sur le c: si il existe pas, ensuite il ajouter les erreurs reçues
        class écrite lors de la survie face au coronavirus
    
    Arguments:
        object {[type]} -- [description]
    """
    
    def __init__(self):
        """init
        
        Arguments:
            txt_error {txt_error} -- json avec l'erreur
            nom_soft {nom_soft} -- nom du soft qui envoie l'erreur
        """

        self.log_path="..\logs_Gets"

    def logging(self,txt_error,nom_soft):
        """si le dossier lof n'existe pas il le crée si le log existe avec le nom du programme il ajoutera de nouvelles lignes avec la date et l'heure du log
        """
        self.txt_error=txt_error
        self.nom_soft=nom_soft

        
        if not os.path.exists(self.log_path) :
            os.makedirs(self.log_path)

        fichier = open(self.log_path+"\\"+self.nom_soft +".log", "a")
        log_string=datetime.now().isoformat(timespec='minutes')  
        log_string=log_string+"\t\t"+self.txt_error+"\r\n"
        fichier.write(str(log_string))
        fichier.close()

    



            


