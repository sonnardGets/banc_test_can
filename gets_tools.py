# -*- coding: utf-8 -*-
"""
@author: Sonnardni
-V4919 correction bug dans thread de programmation.
    Quand un numero d'art n'était pas dans la base de donnée pas de stop du thread. - Maintenant il affiche l'erreur à l'utilisateur et stop le thread
-v5019
    Correction bug thread test maintenant des while attente le retour de l'arduino cela rend le trafique controlé
    ajout de la lecture de version soft et affiche dans le GUI l'infos 
    demande le numero de série à l'électronique et l'affiche dans le GUI
    si l'arduino ignore un data celui-ci est renvoyé 10x --> tempo entre les renvoie ajustée car beaucoup trop rapide avant
    Modification de la lecture d'un code barre, maintenant il affiche l'historique d'un produit
    Sécurtité améliorée lors d'une demande au server d'infos code bar --> si le serveur envoie une erreur on avertit l'utilisateur.
    correction d'un bug sur le numero d'article lors de la réimpression des étiquettes
    correction bug ajout au stock
    correction bug thread fin de test, le soft attendait un data du banc de test mais celui-ci allait pas en envoyer
    optimisation de la lecture du courant
    ajout de la version de soft dans la base de données
    enregistre les log de la console dans un fichier log sur le pc de la personne

-v0320 

    modification de l'impression des étiquettes tests et correction de la compatibilité entre les article générique et GEC
    correction de bug pour l'autocomplétation du champ soft
    ajout d'un champs initials pour le mariage d'un produit afin de garantir une meilleur traçabilité
-v0520

    correction erreur et ajout d'une sécurité si un chemin n'est pas configuré ou faux sur l'interface web on le signal ou on demande de l'entré manuellement si le payload du JSON ne contient pas
    la valeur SofwarePath
- v1120

    le banc test recevra les test et les différentes tâches afin de rendre le GUI plus simple
    - la liste des test sera demandée à google ensuite elle sera envoyée au banc test au format JSON
    - le banc tests fera les testes jusqu'à la fin ou au moment d'un éventuel problème.
    exemple de la structur JSON 
        {"typemsg": "tests","google": {"Boutons": true,"P4": true,"OPTO2-7": true,"Bouton_prise": true}}

"""
import sys
sys.path.insert(0, "..\pythonNS_library")
from PyQt5 import QtCore, QtGui, QtWidgets
from PyQt5.QtWidgets import QMainWindow, QApplication, QWidget, QPushButton, QMessageBox, QInputDialog, QLineEdit, QLabel


from PyQt5.QtCore import pyqtSlot,pyqtSignal, Qt,QSize
from PyQt5.QtGui import QMovie
from PyQt5.uic import loadUi
from threading import Thread, Event
from google_json import *
import asyncio
import json
from queue import Queue
from log import*
from brother import*
from signaux_GUI import*
from etiqueteuse import*
from get_infos_products import*
from Serial_Nicolas import*
from ti_programmer import*
import psutil
from stm_programmer import*
import time
import traceback



class gets_tools(QtCore.QObject):
    """classe permettant cotenant le main thread et permettant la communication entre les diférents élément du programme

    
    Arguments:
        QtCore {[type]} -- [description]
    """

    # signal pour envoyer au terminal du gui
    terminal_test=QtCore.pyqtSignal(str)
    terminal_programation=QtCore.pyqtSignal(str)
    terminal_infos_produits=QtCore.pyqtSignal(str)


    def __init__(self,queue_json,rx_datas_queue,tx_datas_queue,queue_signaux_gui,signaux_gui):
        QtCore.QObject.__init__(self)
        self.stop_threads = Event()
        self.log=log()
        self.google_json=google_tools()
        self.imprimante=imprimante()
        
        self.brother=brother(9100,"192.168.2.192")
        self.infos_products=infos_products()
        self.ti_programmer=ti_programmer()
        self.compteur=0
        self.etiquette_carton_queue = Queue(100)


        self.main_thread_queue = queue_json
        self.queue_signaux_gui=queue_signaux_gui
        self.rx_datas_queue=rx_datas_queue
        self.tx_datas_queue=tx_datas_queue
        self.stm_programmer=stm_programmer(self.main_thread_queue)

    @QtCore.pyqtSlot()
    def main_thread(self):
        run=True
        json_error=""
        path=""
        
        # json de la fonction
        #  {"typemsg": "main_thread","signaux_gui":{"terminal_tests": "string à afficher dans le terminal","terminal_programation": "string à afficher dans le terminal","terminal_Code-barres": "string à afficher dans le terminal"},"tests": {"Boutons": true,"P4": true,"OPTO2-7": true,"Bouton_prise": true}}
        #while not self.stop_threads.is_set():
        # jsons a mettre dans la queue
        # {"typemsg": "GUI","terminal":"tests","txt":" text a afficher"}             le contenu de terminal peut varie  -->   tests      programmation       infos_produits
        #json pour les étiquettes cartons
        # {"typemsg": "carton_labels","infos":{"initials": "ns", quantity}}             le contenu de terminal peut varie  -->   tests      programmation       infos_produits
        while (run==True):
            #self.tx_datas_queue.put("salut")

            #print ("Main thread loop")
            if not self.main_thread_queue.empty():
                #print ("Queue event")
                
                # Si la file est vide, attente qu'elle se remplisse
                input_json=self.main_thread_queue.get(1)
         



                # test si c'est un json car l'arduino envoie des données à l'initialisation pas formatées
                try:

                    json_obj = json.loads(input_json)

                    # controle si typemsg est présent dans le json object
                    if "typemsg" in json_obj:
                                                
                        ########################################
                        #
                        # impression des étiquettes cartons
                        #
                        ########################################

                        # si le json contient une demande d'étiquettes le programme traite et demande directement à google les infos pour les imprimer                               
                        #print (json_obj['typemsg'])
                        if "carton_labels" in json_obj['typemsg']:
                            print ("carton label")
                            print (json_obj)
                            self.imprimante.print_label_carton(json_obj,self.queue_signaux_gui)

                        ########################################
                        #
                        # Start des tests
                        #
                        ########################################

                        if "start_tests" in json_obj['typemsg']:
                            
                            print ("start_tests")
                        #{"typemsg": "signal_gui","signaux_gui":{"terminal_tests": "string à afficher dans le terminal","terminal_programation": "string à afficher dans le terminal","terminal_Code-barres": "string à afficher dans le terminal"},"tests": {"Boutons": true,"P4": true,"OPTO2-7": true,"Bouton_prise": true}}

                            json_start={}
                            json_start ["typemsg"]="GUI"
                            json_start ["terminal"]={
                                "txt":"start"
                            }
                            liste_tests=self.google_json.get_json_test(self.google_json.get_6chfr(json_obj["datas"]["bv"]))
                            print(liste_tests)
                            if "NAK" in liste_tests:
                                json_error={"typemsg":"GUI","warning_box":"Le bv est introuvable"}
                                self.queue_signaux_gui.put(json.dumps(json_error)) 
                            else:
                                print (json_obj["datas"]["bv"])
                                print(liste_tests)
                                self.tx_datas_queue.put(str(liste_tests),1)   
                                self.queue_signaux_gui.put(json.dumps(json_start),1)
                                print("liste in queu")


                        ########################################
                        #
                        # Get infos
                        #
                        ########################################

                        if "get_infos" in json_obj['typemsg']:
                            self.infos_products.get_infos(json_obj['datas']['code-barre'],self.queue_signaux_gui)

                        if "GUI" in json_obj['typemsg']:
                            self.queue_signaux_gui.put(json.dumps(json_obj))

                        # envoie des communications au banc de tests
                        if "comunication" in json_obj['typemsg']:
                            self.queue_signaux_gui.put(json.dumps(json_obj))


                        ########################################
                        #
                        # envoie à la class ti_programmer pour programmer
                        #
                        ########################################
                        if "ti_programming" in json_obj['typemsg']:
                            self.ti_programmer.ti_programmer_soft(json.dumps(json_obj),self.queue_signaux_gui)
                        
                        ########################################
                        #
                        # envoie le numero de serie
                        #
                        ########################################
                        if "no_serie" in json_obj['typemsg']:
                            self.ti_programmer.ti_programmer_soft(json.dumps(json_obj),self.queue_signaux_gui)



                        ########################################
                        #
                        # envoie le resultat test OK /KO
                        #
                        ########################################
                        if "result" in json_obj['typemsg']:
                            print (json_obj)
                            if "global_tests" in json_obj['result']:
                                if "true" in json_obj['result']['global_tests']:
                                    json_box={}
                                    json_box ["typemsg"]="GUI"
                                    json_box ["ok_box"]="Bravo tous les tests sont ok"
                                    self.queue_signaux_gui.put(json.dumps(json_box))            
                                

                                if "false" in json_obj['result']['global_tests']:
                                    json_box={}
                                    json_box ["typemsg"]="GUI"
                                    json_box ["warning_box"]="Test KO"
                                    self.queue_signaux_gui.put(json.dumps(json_box))            


                        ########################################
                        #
                        # envoie la programation au produit STLINK
                        #
                        ########################################
                        if "st_programming" in json_obj['typemsg']:

                            json_serie={"reset_stlink":"true"}
                            self.tx_datas_queue.put(json.dumps(json_serie))
                            time.sleep(5)
                            # test si le GUI envoie un ordre
                            if "numero" in json_obj:
                                if len(json_obj["numero"])<5:
                                    json_error={"typemsg":"GUI","warning_box":"Numéro de série saisis trop petit"}
                                    self.queue_signaux_gui.put(json.dumps(json_error)) 
                                else:
                                    #demande les infos au cloud
                                    try:
                                        path=self.google_json. return_path_json(json_obj["numero"])['softwarePath']
                                        
                                        print(path)
                                        json_soft={"typemsg":"GUI","label_name_soft":path}
                                        self.queue_signaux_gui.put(json.dumps(json_soft)) 
                                        if "error" in path:
                                            json_error={"typemsg":"GUI","warning_box":path}
                                            self.queue_signaux_gui.put(json.dumps(json_error)) 
                                        else:
                                            asyncio.run(self.stm_programmer.start_prog(path))
                                        # on fait un reset du pgramateur car parfois il bug
                                        json_serie={"reset_stlink":"true"}
                                        self.tx_datas_queue.put(json.dumps(json_serie))
                                        time.sleep(5)

                                        # regarde si le path numero 2 est existant
                                        path2=self.google_json.return_path_json(json_obj["numero"])      
                                        json_soft={"typemsg":"GUI","label_name_soft":path2}
                                        self.queue_signaux_gui.put(json.dumps(json_soft)) 

                                        print ("path 2 --->")
                                        print (path2)
                                        if 'softwarePath2' in path2:         
                                            if len(path2['softwarePath2'])>=10:
                                                path=path2['softwarePath2']
                                                if "error" in path:
                                                    json_error={"typemsg":"GUI","warning_box":path}
                                                    self.queue_signaux_gui.put(json.dumps(json_error)) 
                                                else:
                                                    asyncio.run(self.stm_programmer.start_prog(path))
                                    except:
                                            print(traceback.format_exc())
                                            json_error={"typemsg":"GUI","warning_box":"chemin de soft introuvable, vérifez sur le cloud \r\n" + str(traceback.format_exc())}
                                            self.queue_signaux_gui.put(json.dumps(json_error)) 



                            
                        

                

                except:
                    print(traceback.format_exc())
                    self.log.logging(str(traceback.format_exc()),"gets_tools")

                ########################################
                #
                # cpu to gui
                #
                ########################################
                self.compteur+=1
                if self.compteur==10:
                    json_cpu={}
                    json_cpu ["typemsg"]="GUI"
                    json_cpu ["cpu"]=psutil.cpu_percent()
                    #parfois python ne peut pas accéder au cpu donc si il reçoit 0 on affiche rien
                    if json_cpu['cpu']>0:
                        #print("CPU")
                        self.compteur=0
                        self.queue_signaux_gui.put(json.dumps(json_cpu))



    def GetInfos(self,NoCodeBar):
        """lit les infos du code barre et les envoie au GUI pour ensuite les afficher 
        """
        


        
        #si la chaine est plus grande que 11 c est le lecteur de code bar alors on enleve le checksum
        if len (NoCodeBar)>11:

            NoCodeBar=NoCodeBar[:-1]
            NoCodeBar=NoCodeBar[5:15]


        
        self.lineEdit_3.clear()
        #URLInfos="https://us-central1-fabrication-8bd49.cloudfunctions.net/htmlGetLabelData?token=test&barcode="+NoCodeBar
        URLInfos="https://us-central1-fabrication-8bd49.cloudfunctions.net/getSerialNumberLogs?serialnumber="+NoCodeBar+"&token=dl2ud5djezewbfj363fdmj37edfcj387wefwjsdvdwrdgfgkhlzi463rfdcmsfhg"

        # parse le Json pour envoyer le resultat à l'etiqueteuse
        getresult = requests.get(URLInfos)
        # some JSON:
        x=getresult.text

        # test si le numero existe dans la base de données
        if "NAK" in x or "Error" in x:
            json_box={}
            json_box ["typemsg"]="GUI"
            json_box ["warning_box"]="numero inexistant base de donnée"
            self.queue_signaux_gui.put(json.dumps(json_box))            
            return

        listJSON = json.loads(x)
        #listx=list(x.split("[]")) 
        for JSON in listJSON:
            print ("\r\n")
            #print (JSON)
            self.listWidget_5.addItem("------------------------------------------------------------------------------------------------------------------------")
            print("------------------------------------------------------------")
            #y = json.dumps(JSON)
            for key, content in JSON.items():
                # test si le payload creationTime est présent
                
                if  "creationTime" in key:
                    JsonTime=JSON.get("creationTime")
                    #JsonTime=JsonTime.decode(encoding)
                    JsonTime=str(JsonTime)
                    JsonTime=JsonTime.replace("u'_",'"')
                    JsonTime=JsonTime.replace("'",'"')


                    time=json.loads(JsonTime)

                    timestamp = datetime.datetime.fromtimestamp(time ["_seconds"])
                    DateProd=(timestamp.strftime('%d-%m-%Y  %H:%M:%S'))
                    print("date " + "\t\t"+ DateProd)
                    self.listWidget_5.addItem("date " + "\r\n\t\t\t:\t"+ DateProd)
                    #del JSON ["creationTime"]
                else:
                    self.listWidget_5.addItem(str(key)+"\r\n\t\t\t:\t"+str(content))
                    print(str(key)+"\t:\t"+str(content))


