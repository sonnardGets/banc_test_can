'''
Filename: c:\pythonNS_library\get_infos_products.py
Path: c:\pythonNS_library
Created Date: Thursday, May 14th 2020, 11:15:01 am
Author: Nicolas.Sonnard

Copyright (c) 2020 Your Company
'''
import datetime
import requests
import json
date = datetime.datetime.now()


class infos_products(object):

    def __init__(self):
        print ("infos_products")


    def send_json_GUI(self,txt):
        json_to_gui={}
        json_to_gui ["typemsg"]="GUI"
        json_to_gui ["terminal_5"]={
            "txt":str(txt)
        }    
        self.queue_signaux_gui.put(json.dumps(json_to_gui))


    def get_infos(self,NoCodeBar,queue_signaux_gui):
        self.queue_signaux_gui=queue_signaux_gui


        
        #si la chaine est plus grande que 11 c est le lecteur de code bar alors on enleve le checksum
        if len (NoCodeBar)>11:

            NoCodeBar=NoCodeBar[:-1]
            NoCodeBar=NoCodeBar[5:15]


        
      
        #URLInfos="https://us-central1-fabrication-8bd49.cloudfunctions.net/htmlGetLabelData?token=test&barcode="+NoCodeBar
        URLInfos="https://us-central1-fabrication-8bd49.cloudfunctions.net/getSerialNumberLogs?serialnumber="+NoCodeBar+"&token=dl2ud5djezewbfj363fdmj37edfcj387wefwjsdvdwrdgfgkhlzi463rfdcmsfhg"

                # parse le Json pour envoyer le resultat à l'etiqueteuse
        getresult = requests.get(URLInfos)
        # some JSON:
        x=getresult.text

        # test si le numero existe dans la base de données
        if "NAK" in x or "Error" in x:
            QMessageBox.warning(self, 'Erreur'," Sorry ! Numero inexistant dans le system ou une erreur c'est prduite:-(")
            return

        listJSON = json.loads(x)
        #listx=list(x.split("[]")) 
        for JSON in listJSON:
            print ("\r\n")
            #print (JSON)
            self.send_json_GUI("------------------------------------------------------------------------------------------------------------------------")
            print("------------------------------------------------------------")
            #y = json.dumps(JSON)
            for key, content in JSON.items():
                # test si le payload creationTime est présent
                
                if  "creationTime" in key:
                    JsonTime=JSON.get("creationTime")
                    #JsonTime=JsonTime.decode(encoding)
                    JsonTime=str(JsonTime)
                    JsonTime=JsonTime.replace("u'_",'"')
                    JsonTime=JsonTime.replace("'",'"')


                    time=json.loads(JsonTime)

                    timestamp = datetime.datetime.fromtimestamp(time ["_seconds"])
                    DateProd=(timestamp.strftime('%d-%m-%Y  %H:%M:%S'))
                    print("date " + "\t\t"+ DateProd)
                    self.send_json_GUI("date " + "\r\n\t\t\t:\t"+ DateProd)
                    #del JSON ["creationTime"]
                else:
                    self.send_json_GUI(str(key)+"\r\n\t\t\t:\t"+str(content))
                    print(str(key)+"\t:\t"+str(content))