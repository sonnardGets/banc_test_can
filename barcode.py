# -*- coding: utf-8 -*-
'''
barcode reader
Author: Nicolas.Sonnard

classe permettant de lire les code bar et renvoie la valeur correct 
enlève le checksum et les débuts de chaines
dans le cas ou un numéro est saisis manuellement la classe devra valider ce numéro.


Copyright (c) 2020 Your Company
'''

class barcode_reader (object):

    '''
    bv
    000002579276
    mac
    0103468683849380
    no_serie
    0100016190014635
    no6chr
    000001100341
    '''

    def number_barcode(self,numero):

        # detect si c'est un numero de bv ou numero article
        if len(numero)==12:
            #si la chaine est plus grande que 11 c est le lecteur de code bar alors on enleve le checksum
            numero=numero[:len(numero)-1]
            numero=numero.lstrip("0")
            return numero
        # detect si c'est une étiquette          
        if len(numero)==16:
            #permet d'enlever le 1 au début de l'étiquette du codebarre
            numero=numero[2:len(numero)-1]
            numero=numero.lstrip("0")
            return numero

        # si aucune correspondance
        if len(numero)>16 and len(numero)<6:
            return "error code barre"

        #return du numero car c'est pas un code barre et pas une erreure.
        return numero
            


