
'''
Filename: c:\pythonNS_library\version_soft_calculator.py
Path: c:\pythonNS_library
Created Date: Wednesday, May 13th 2020, 4:03:13 pm
Author: Nicolas.Sonnard

Copyright (c) 2020 Your Company
'''

import os
import datetime
import time

class soft_calculator(object):

    def __init__(self):
        date = datetime.datetime.now()


    def VersionSoftCalculator(self, path):
        annee=int((time.strftime("%Y",time.gmtime(os.path.getmtime(path)))))
        mois=int((time.strftime("%m",time.gmtime(os.path.getmtime(path)))))
        jour=int((time.strftime("%d",time.gmtime(os.path.getmtime(path)))))

        no_semaine=datetime.date(annee,mois,jour).isocalendar()[1]
        annee=str(annee)
        if int(no_semaine)<10:
            return "V0"+str(no_semaine)+annee[2]+annee[3]
        return "V"+str(no_semaine)+annee[2]+annee[3]



 