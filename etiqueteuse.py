'''
Filename: c:\pythonNS_library\etiqueteuse.py
Path: c:\pythonNS_library
Created Date: Thursday, April 30th 2020, 6:19:30 pm
Author: Nicolas.Sonnard

Copyright (c) 2020 Your Company
'''

"""getion de l'imprimante d'etiquettes
"""
from google_json import*
from brother import*
from signaux_GUI import*
import configparser
import json

class imprimante(object):



    def __init__(self):
        
        self.google_json=google_tools()
        #signaux_gui=signaux_gui()
        self.config = configparser.ConfigParser()
        self.config.read(r'./config/config.cfg')
        self.brother=brother(self.config.getint('brother', 'port'),self.config.get('brother', 'ip'))
        print ("initialisation de l'étiqueteuse  "+self.config.get('brother', 'ip')+":"+str(self.config.getint('brother', 'port')))
    


             


    def print_label_carton(self,json_gui,queue_signaux_gui):
        """envoie à l'étiqueteuse brother les étiquettes à imprimer

        Arguments:
            json_gui {string} -- [json venant du gui avec les champs de l'utilisateur]
            queue_signaux_gui {[queue]} -- [queue qui enverra les infos au gui]
        """
        self.queue_signaux_gui=queue_signaux_gui

        json_obj=json_gui
        print ("print_label_carton")
        #try:
        print (self.google_json.get_json_carton(json_obj["datas"]["bv"],json_obj["datas"]["initials"]))
        json_obj_cartons = json.loads(self.google_json.get_json_carton(json_obj["datas"]["bv"],json_obj["datas"]["initials"]))
        print (json_obj_cartons)
        self.brother.init_printer()
        self.brother.select_template("001")
        self.brother.edit_template(json_obj_cartons["partNumber"],"no_art")
        print (json_obj_cartons["partNumber"])
        self.brother.edit_template(json_obj_cartons["description"],"description")
        print (json_obj_cartons["description"])
        self.brother.edit_template("http://gets.ch","qr")
        self.brother.edit_template(str(json_obj_cartons["boxedQuantity"])+"/"+str(json_obj_cartons["requestedQuantity"]),"qty")
        print (json_obj["datas"]["bv"])
        self.brother.edit_template(json_obj["datas"]["bv"],"bv")
        print (json_obj["datas"]["bv"])
        self.brother.edit_template(json_obj["datas"]["initials"],"initials")
        print (json_obj["datas"]["initials"])
        self.brother.edit_template(" ","no_semaine")
        self.brother.edit_template("000000"+str(json_obj_cartons["barcode"]),"barre_code")
        print (str(json_obj_cartons["barcode"]))
        self.brother.print_label() 

        json_to_gui={}
        json_to_gui ["typemsg"]="GUI"
        json_to_gui ["carton_infos"]={
            "boxedQuantity":str(json_obj_cartons["boxedQuantity"]),
            "requestedQuantity":str(json_obj_cartons["requestedQuantity"]),
            "infos_product":str(json_obj_cartons["description"])
        }    
        self.queue_signaux_gui.put(json.dumps(json_to_gui))   
     