'''
Filename: c:\pythonNS_library\ti_programmer.py
Path: c:\pythonNS_library
Created Date: Monday, June 8th 2020, 2:02:21 pm
Author: Nicolas.Sonnard

Copyright (c) 2020 Your Company
afin de simplifier le soft et du fait que c'est un produit en fin de vie les chemins seront mis dans le fichier configparser
'''
import os
import sys
import json
import subprocess


class ti_programmer(object):

    def __init__(self):
        print ("ti_programmer_class")

    def ti_mac_write(self,mac_decimal):

            # test si l'addresse mac est un code bar
            if len (mac_decimal)>=16:
                # enleve le checksum et le début du code bar
                mac_decimal=mac_decimal[3:-1]
            

            #convertion de l'adresse mac décimal en une adresse 00-50-10-22-22
            
            adresse_mac=int(mac_decimal)
            adresse_mac=hex(adresse_mac)[2:]
            #adresse_mac=str(adresse_mac) 
            adresse_mac_finale="00-"+str(adresse_mac[0:2])+"-"+str(adresse_mac[2:4])+"-"+str(adresse_mac[4:6])+"-"+str(adresse_mac[6:8])+"-"+str(adresse_mac[8:10])
            print("programmation de l'adresse MAC")
            print (adresse_mac_finale)


        
    def ti_programmer_soft(self,input_json,queue_signaux_gui):
        """programme le soft sur un uc TI

        Args:
            json (strin): json contenant les infos pour la prog {"typemsg":"ti_programming","path":"\\192.168.2.8\folder…."}

        """
        self.queue_signaux_gui=queue_signaux_gui
        print ("json signal")
        print (input_json)
        json_obj = json.loads(str(input_json))

        # conversion des chemin \\ -> // 
        self.softwarePath=json_obj["path"]
        self.softwarePath=self.softwarePath.replace("\\","/")
        Progstat=False
        pourcent_programming=False 
        TimeOut=0
        fichier=os.listdir(self.softwarePath)[0]
        cmd=""

        while Progstat==False and TimeOut<=5:
            process = subprocess.Popen("lmflash -q manual -i ICDI --debug-port JTAG -s 2000000 -v -r "+'"'+self.softwarePath+"/"+fichier+'"', shell=True,stdout=subprocess.PIPE,stderr=subprocess.STDOUT)
            print ("lmflash -q manual -i ICDI --debug-port JTAG -s 2000000 -v -r "+'"'+self.softwarePath+"/"+fichier+'"')
            '''
            Commandeshell="lmflash -q manual -i ICDI --debug-port JTAG -s 2000000 -v -r "+'"'+self.softwarePath+"/"+fichier+'"'
            process = os.popen(Commandeshell)
            print (Commandeshell)
            #print (Commandeshell)

            #2   for c in iter(lambda: process.stdout.read(100), ''):
            
            cmd=process.read()
            '''
            if "mac" in json_obj:
                self.ti_mac_write(json_obj['mac'])
            for line in process.stdout.read(10):
                #print(line)

                json_ti={}
                json_ti ["typemsg"]="GUI"
                json_ti ["ti_terminal"]="."
                self.queue_signaux_gui.put(json.dumps(json_ti))
                if '\r' in str(line):
                    pourcent_programming=True 
                
                if pourcent_programming==True:
                    cmd=cmd+str(line)
                
                if "Verify Complete - Passed" in cmd:
                    print ("PASSED OK")
                    Progstat=True
                    json_ti={}
                    json_ti ["typemsg"]="GUI"
                    json_ti ["ti_state"]="OK"
                    self.queue_signaux_gui.put(json.dumps(json_ti))
                    
                if "ERROR":
                    json_ti={}
                    json_ti ["typemsg"]="GUI"
                    json_ti ["ti_state"]="KO"
                    self.queue_signaux_gui.put(json.dumps(json_ti))
                    return



            TimeOut+=1
            if TimeOut ==5:
                json_ti ["typemsg"]="GUI"
                json_ti ["ti_terminal"]="Timeout erreur"





    '''
        while Progstat==False and TimeOut<=5:
            process = subprocess.Popen("lmflash -q manual -i ICDI --debug-port JTAG -s 2000000 -v -r "+'"'+self.softwarePath+"/"+fichier+'"', stdout=subprocess.PIPE,stderr=subprocess.PIPE)
            for c in iter(lambda: process.stdout.read(50), ''):
                sys.stdout.write(str(c))
                print (c)
                if "Passed" in c:
                    print ("PASSED OK")
                    Progstat=True
                else:
                    Progstat=False
            TimeOut+=1
        if Progstat==True:
            print ("prog OK")
            
        
        if Progstat==False:
            print ("prog KO")

        '''


        
 