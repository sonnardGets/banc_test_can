'''
Filename: c:\pythonNS_library\printer.py
Path: c:\pythonNS_library
Created Date: Monday, March 16th 2020, 3:46:35 pm
Author: Nicolas.Sonnard

class gérant l'étiqueteuse de l'atelier pour les étiquettes petites et fines


Copyright (c) 2020 Your Company
'''
import requests
import time
from PyQt5.QtGui import *
from PyQt5.QtWidgets import *
import os
import json
import configparser
from google_json import*



class printer(QWidget):
    """classe permettant d'imprimer les etiquettes



    Arguments:
        object {[voir fonctions]} -- [description]
    """

    def __init__(self, parent = None):
        QWidget.__init__(self, parent)
        """init de la class
        """
        test = 0
        self.config = configparser.ConfigParser()
        self.config.read(r'./config/config.cfg')
        self.IpEtiqueteuse=str(self.config.get('cab_labels', 'ip'))
        self.etiquette_ip=str(self.config.get('cab_labels', 'etiquette_ip'))
        self.etiquette_simple=str(self.config.get('cab_labels', 'etiquette_simple'))
        self.version_soft=self.config.get('user_settings', 'name')        
        

    def Get6chfr(self,NoCodeBar):
        '''
        000002090610
        000001024432
        '''
        if ("stock" in NoCodeBar):

            return "stock"



        if len(NoCodeBar)<6:    
            return "NAK"
        if len(NoCodeBar)>6:
            NoCodeBar=NoCodeBar[:-1]
            NoCodeBar=NoCodeBar.lstrip('0')
            '''
            000021635212
            000001100242
            '''

        print(NoCodeBar)
        return NoCodeBar


    async def imprimerAvecBv (self,bv,no6chfr,quantity,soft,commentaire,initials,queue_signaux_gui):

                

        # on test si il y a pas de text dans bv 
        if len(bv)==0:
            Bv="0"
            # il y a pas de text dans bv donc envoie d'un no d'article mannuellement avec controle de minimum 6 chiffre sinon on sort            
            if len(no6chfr)<6:
                queue_signaux_gui.put()
                QMessageBox.critical( self,"ERROR", "pas de numéro à 6 chiffre inscrit /r/n et pas de numero de bv détécté")
                return
            if len(no6chfr)<5:
                QMessageBox.critical( self,"ERROR", "Numéro de bv saisi trop petit")
                return
        # contient les multiple test des champs selon une logic bv ou pas bv ... lire pour comprendre ;-)
        commentaire= "   "       
        
        if len(no6chfr)==0:
            if len(bv)==0:
                QMessageBox.critical( self,"ERROR", "Si il y a pas de numero à 6 chiffre il faut mettre un BV  ")
            no6chfr=1234
        else:
            # on deduit qu'un numero 6 chfr est valide si il est plus grand que 11 car c'est qu'il a été scanné donc faut enlever le checksum et le début inutil...
            if len (no6chfr)>11:
                #on enlève le checksum
                no6chfr=no6chfr[:-1]
                no6chfr=no6chfr[5:len(no6chfr)]

        #  on force une quantité à 0 si un bv est scanné car la request plus bas fournira ces infos         
        if len(quantity)==0:
            quantity=="0"
        

        else:
            # on deduit qu'un numero de bv est valide si il est plus grand que 11 car c'est qu'il a été scanné donc faut enlever le checksum et le début inutil...
            if len (bv)>11:
                #on enlève le checksum
                
                bv=bv.lstrip("0")
                bv=bv[:-1]

        # cherche le soft selon le bv ou le numero à 6 chiffre
        if len (no6chfr)>0 and len (no6chfr)<15:
            URL="https://us-central1-fabrication-8bd49.cloudfunctions.net/getProductFamilyData?token=dl2ud543fdfjkvbi46sdsyvxkvb%C3%A9lji06ufbcmsfhg&produit="+str(self.Get6chfr(no6chfr))


        if len (bv)>0:
            URL="https://us-central1-fabrication-8bd49.cloudfunctions.net/getProductFamilyData?token=dl2ud543fdfjkvbi46sdsyvxkvb%C3%A9lji06ufbcmsfhg&produit="+str(self.Get6chfr(bv))
        
        getresult = requests.get(URL)
        
        # some JSON:
        x=getresult.text
        print (no6chfr)
        print(URL)
        print (x)
        # test si le produit est existant
        if "NAK"in x :
            print ("NAK detecte")
            if len(soft)==0:
                soft_dialog, ok = QInputDialog.getText(self, 'Soft', 'Entrer une version de soft:')
                if ok:
                    if len(soft_dialog)<3:
                        return
                    else:
                        soft=soft_dialog                        
            

    
        if not "NAK" in x:
            y = json.loads(x)
            self.softwarePath=""

            #reprend les valeur parse du json pour les envoye dans la base de donee des no de serie
            

            #G test si il y a un soft dans le json
            if "softwarePath" in x:
                self.softwarePath=y.get("softwarePath")
                self.softwarePath=self.softwarePath.replace("\\","/")
                if not os.path.isdir(self.softwarePath):
                    QMessageBox.critical( self,"ERROR", "Chemin soft invalid")  
                    return

                fichier=os.listdir(self.softwarePath)[0]
                soft=fichier                 
                QMessageBox.about( self,"soft", "Une version de soft a été trouvée sur le réseau \r\n "+str(soft))  

            else:
                QMessageBox.about( self,"infos", "Quelque chose c'est mal passé, si ce message revient plusieurs fois il faut appeler Nicolas \r\n Pas de soft configuré dans la base de données")  
                return

            self.familyproduct=y.get("familyName")
            
                
            print ("infos du json")
            print (self.softwarePath)
            print (self.familyproduct)       
        
        

        


        if len(initials)==0:
            QMessageBox.critical( self,"ERROR", "Merci de remplire les initials ou un nom ")
            return
        print ("envoye à google étiquettes"+str (no6chfr+ quantity+ bv+ soft+ commentaire+ initials))
        self.google_etiquettes(no6chfr, quantity, bv, soft, commentaire, initials)
        



 

    def PrintEtiquette(self, ligne1, No6chiffre, ligne2, nbetiquette, FormatEtiquette):

        # selection de l etiquette
        No6chiffre = str(No6chiffre).lstrip("0")
        #No6chiffre="Art : "+No6chiffre
       


        xml = """<?xml version="1.0" ?>
        <S:Envelope xmlns:S="http://schemas.xmlsoap.org/soap/envelope/">
        <S:Body><loadFormatRequest xmlns="http://www.cab.de/WSSchema">"""+FormatEtiquette+"""</loadFormatRequest></S:Body></S:Envelope>
        """
        headers = {
            'Content-Type': 'application/xml'}  # set what your server accepts
        print(requests.post('http://'+self.IpEtiqueteuse +
                            '/cgi-bin/soap/printerservice/', data=xml, headers=headers).text)

        xml = """<?xml version="1.0" ?>
        <S:Envelope xmlns:S="http://schemas.xmlsoap.org/soap/envelope/">
            <S:Body><printFormatRequest xmlns="http://www.cab.de/WSSchema">
                <objects><fObject><name>article</name><value>"""+No6chiffre+"""</value></fObject>
                <fObject><name>serial</name><value>"""+str(ligne1)+"""</value></fObject>
                <fObject><name>BarCode</name><value>"""+str(ligne2)+"""</value></fObject>
                </objects>
                <jobID>CAN</jobID><numbers>"""+str(nbetiquette)+"""</numbers></printFormatRequest>
                </S:Body></S:Envelope>"""
        headers = {
            'Content-Type': 'application/xml'}  # set what your server accepts
        print(requests.post('http://'+self.IpEtiqueteuse +
                            '/cgi-bin/soap/printerservice/', data=xml, headers=headers).text)

    def SelectEtiquette(self, FormatEtiquette):

        xml = """<?xml version="1.0" ?>
        <S:Envelope xmlns:S="http://schemas.xmlsoap.org/soap/envelope/">
        <S:Body><loadFormatRequest xmlns="http://www.cab.de/WSSchema">"""+FormatEtiquette+"""</loadFormatRequest></S:Body></S:Envelope>
        """
        headers = {
            'Content-Type': 'application/xml'}  # set what your server accepts
        rep = requests.post(
            'http://'+self.IpEtiqueteuse+'/cgi-bin/soap/printerservice/', data=xml, headers=headers).text


    def PrintEtiquetteIP(self,CodeBarre,Hostname, MacAdresse,ligne3,nbetiquette ):


        xml = """<?xml version="1.0" ?>
        <S:Envelope xmlns:S="http://schemas.xmlsoap.org/soap/envelope/">
            <S:Body><printFormatRequest xmlns="http://www.cab.de/WSSchema">
                <objects><fObject><name>article</name><value>"""+Hostname+"""</value></fObject>
                <fObject><name>serial</name><value>"""+str(MacAdresse)+"""</value></fObject>
                <fObject><name>BarCode</name><value>"""+str(CodeBarre)+"""</value></fObject>
                </objects>
                <jobID>CAN</jobID><numbers>"""+str(nbetiquette)+"""</numbers></printFormatRequest>
                </S:Body></S:Envelope>"""
        headers = {'Content-Type': 'application/xml'} # set what your server accepts
        rep= requests.post('http://'+self.IpEtiqueteuse+'/cgi-bin/soap/printerservice/', data=xml, headers=headers).text



    def PrintEtiquetteOld(self,ligne1,No6chiffre, ligne2,ligne3,nbetiquette ):



        # selection de l etiquette
        No6chiffre= str(No6chiffre).lstrip("0")
        # si le len de no 6 chiffre est plus petit que 7 on ajoute Art devant --> modife pour utili

        No6chiffre="Art : "+No6chiffre





        xml = """<?xml version="1.0" ?>
        <S:Envelope xmlns:S="http://schemas.xmlsoap.org/soap/envelope/">
            <S:Body><printFormatRequest xmlns="http://www.cab.de/WSSchema">
                <objects><fObject><name>article</name><value>"""+No6chiffre+"""</value></fObject>
                <fObject><name>serial</name><value>"""+str(ligne1)+"""</value></fObject>
                <fObject><name>BarCode</name><value>"""+str(ligne2)+"""</value></fObject>
                </objects>
                <jobID>CAN</jobID><numbers>"""+str(nbetiquette)+"""</numbers></printFormatRequest>
                </S:Body></S:Envelope>"""
        headers = {'Content-Type': 'application/xml'} # set what your server accepts
        rep= requests.post('http://'+self.IpEtiqueteuse+'/cgi-bin/soap/printerservice/', data=xml, headers=headers).text





    def google_etiquettes(self, no6chfr, quantity, bv, soft, commentaire, initials):

        """lance à l'imprimantes les différantes étiquettes doit être un thread qui attend

        Arguments:
            no6chfr {int} -- integer contenant le numero à 6 chfr mettre 0 si il y a un bv
            quantity {int} -- integer contenant le numero à 6 chfr mettre 0 si il y a un bv
            bv {int} -- integer contenant le bv mettre 0 si il y a pas de bv
            soft {string} -- contient le soft simple string donc libre --> va ensuite dans la base de données
            commentaire {string} -- contient un éventuel commentaire sur la pièce produite --> va ensuite sur la base de données
            initials {string} -- doit avoir au minimum 2 caractère car ce sont des initials
        """
        print ("google_etiquettes")
        self.no6chfr=no6chfr
        self.quantity=quantity
        self.bv=bv
        self.soft=soft
        self.commentaire=commentaire
        self.initials=initials
        try:
            long6chfr=int(self.no6chfr)
        except:
            QMessageBox.warning( self,"Erreur", "ton numéro de bv semble faux, impossible de continuer ")
            return

        print (self.no6chfr)
        print (initials)

        if len(self.bv) > 1:
            if int(self.bv) > 1000:
                # get du json contenant les infos du Bv ex no article + quantite
                print("bv\r\n\t\t\t\t\t\t\t"+self.bv)
                URL = "https://www.gets-support.ch/dkeuegksowiwmndjkzsh/dkeugcjfiezwkoezwhxksnaheuf.php?barcode="+self.bv
                JsonBv = requests.get(URL)
                x = JsonBv.text
                if "NAK" in x:
                    print("NAK étiquette inconnue !!")
                    return
                y = json.loads(x)

                # reprend les valeur parse du json pour les envoye dans la base de donee des no de serie
                self.quantity = str(y["quantite"])
                self.no6chfr = str(y["noarticle"])
                print("infos du json")
                print(self.quantity)
                print(self.no6chfr)

            print("binch")
            long6chfr = int(self.no6chfr)

        if long6chfr > 110000:
            # URL avec serie qui ont physiquement un numero de série interne --> à partir de 110000
            print("numero > 110xxx \r\n\t\t\t\t\t\t\t"+self.no6chfr)

            URL = "https://www.gets-support.ch/article/?article=" + \
                str(self.no6chfr)
            InfosProduit = requests.get(URL)
            #URL= "https://us-central1-fabrication-8bd49.cloudfunctions.net/printProductLabels?quantity="+self.quantity+"&produit="+str(self.no6chfr)+"&token=dkwu3rdgcm3856fbnwk37fhg"+"&utilisateur="+self.initials+"&produit="+str(self.no6chfr)+"&BV="+self.bv+"&version="+self.soft+"&comment="+self.commentaire+"&produit_txt="+InfosProduit.text

            URL = "https://us-central1-fabrication-8bd49.cloudfunctions.net/printProductLabels?quantity="+self.quantity+"&produit=" + \
                str(self.no6chfr)+"&utilisateur="+self.initials+"&BV="+self.bv+"&version="+self.soft+"&comment="+self.commentaire + \
                "&soft_Banc_tests"+self.version_soft+"&produit_txt="+InfosProduit.text + \
                "&token=dkwu3rdgcm3856fbnwk37fhg"
        else:
            # demande les infos du produit selon le no a 6 chiffres
            print("numero < 110xxx\r\n\t\t\t\t\t\t\t"+self.no6chfr)
            URL = "https://www.gets-support.ch/article/?article=" + \
                str(self.no6chfr)
            InfosProduit = requests.get(URL)
            URL = "https://us-central1-fabrication-8bd49.cloudfunctions.net/htmlPrintlabels"+"?quantity="+str(self.quantity)+"&token=test&family=generic&utilisateur="+self.initials+"&produit="+str(
                self.no6chfr)+"&BV="+self.bv+"&version_soft="+self.soft+"&comment="+self.commentaire+"&produit_txt="+InfosProduit.text
            print("infos du produit \r\n\t\t\t\t\t\t\t"+str(InfosProduit))
            print(InfosProduit)

        getresult = requests.get(URL)

        # some JSON:
        x = getresult.text
        print("resultat get")
        print(x)
        if "NAK" in x or "Product not found" in x:

            QMessageBox.warning( self,"Erreur", "le produit est introuvable dans la base de données ")
            
            return
        else:

            y = json.loads(x)
            # affiche toutes les valeurs du dictionnaire parsé
            # test si on voit le mot labels dans l'étiquette, cela signifie qu'il faut parsé une deuxième fois
            if "labels" in y:
                y = y["labels"]

            self.SelectEtiquette(self.etiquette_simple)
            # envoie les parametres dans une liste pour que le thread puisse la traiter
            for NoCodeBar, NoSerie in y.items():

                print(NoCodeBar)
                print(NoSerie)
                self.PrintEtiquetteOld(str(NoCodeBar), str(
                    self.no6chfr), str(NoCodeBar), "", 1)
            URL = "https://us-central1-fabrication-8bd49.cloudfunctions.net/getProductData?produit=" + \
                self.no6chfr+"&token=dkwuekzqenvloezvbxfhg"
            JsonBv = requests.get(URL)
            JsonTest = JsonBv.text

            # impression des étiquettes avec la mac adresse si le test network est présent la quantité est sortie de Json plus haut.
            if "Network" in JsonTest:

                self.SelectEtiquette(self.etiquette_ip)
                print("impression mac adresse")
                MacDico = {}
                for y in range(int(self.quantity)):
                    URL = "https://us-central1-fabrication-8bd49.cloudfunctions.net/htmlGetMacAddres?token=dkeuwhck3875hfk3947dj29d7h"
                    MAC_JSON = requests.get(URL)
                    MAC = MAC_JSON.text
                    MAC = MAC.upper()
                    MacDico[y] = str(MAC)

                for x in range(int(self.quantity)):

                    MACCodeBarre = MacDico[x].replace('-', '')
                    MacUrl = "http://gets_" + \
                        MACCodeBarre[8]+MACCodeBarre[9] + \
                        MACCodeBarre[10]+MACCodeBarre[11]
                    print(MACCodeBarre)
                    print(MacUrl)
                    NoCodeBar = int(MACCodeBarre, 16)
                    print(MAC)
                    # def PrintEtiquetteIP(CodeBarre,Hostname, MacAdresse,ligne3,nbetiquette ):
                    self.PrintEtiquetteIP(str(NoCodeBar), str(
                        MacUrl), str(MacDico[x]), "", 1)

            # ListCodeBar.append(NoCodeBar)
            # stocke le nombre de no a 6 chiffre --> plus simple de faire comme ça
            # ListNo6Chiffre.append(NumeroArt)
            QMessageBox.about( self,"infos", "Tes étiquettes sont prêtes !")  

