
#!/usr/bin/env python

# -*- coding: utf-8 -*-

'''
Filename: c:\pythonNS_library\brother.py
Path: c:\pythonNS_library
Created Date: Thursday, April 23rd 2020, 1:39:25 pm
Author: Nicolas.Sonnard

Copyright (c) 2020 Your Company


Class permetant d'imprimer les étiquettes à la production sur les petites imprimante brother
afin de simplifier le déploiement l'imprimante fonctionnera en ip avec le protocol tcp sur le port 9100
'''
import socket
import asyncio



class brother(object):
    """Class permetant d'imprimer les étiquettes à la production sur les petites imprimante brother
    
    Arguments:
        object {[type]} -- [description]
    """

    def __init__(self,port,ip_printer):
        """[init de la classe]
        
        Arguments:
            socket {int} -- port de l'imprimante
            ip_printer {string} -- ip de l'imprimante
        """
        self.tcp_port=port
        self.ip_printer=ip_printer
        print ("envoie des datas à l'imprimante " + ip_printer)

    def socket_connect(self):
        """connexion au socket de l'imprimante
        
        Returns:
            int -- objet du socket
        """
        sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        sock.connect((self.ip_printer, self.tcp_port))
        return sock
        
    def tcp_send(self, datas):
        """envoie les datas et connect le socket de l'imprimante
        
        Arguments:
            datas {string} -- string contenant les datas de l'imprimante
        """
        tcpsocket=self.socket_connect()
        tcpsocket.send(datas)  # encode en windows 1252 l'imprimante aime bien ce format ;-)
        tcpsocket.close()

    def init_printer(self):      
        """initialisation de l'imprimante
        """
        self.tcp_send(b"\x1b\x69\x61\x33")        # init printer et changement du mode ( commande via ptouch ^)
        self.tcp_send(str.encode("^II"))          # init via commande

    def select_template(self, template):
        """selection du template à imprimer attention a configurer le template avec le soft de brother
        
        Arguments:
            template {strin} -- nom du template dans l'imprimante
        """
        self.tcp_send(str.encode("^ON"+template+chr(0))) 
        
    def edit_template(self, text, template):
        """edit le template avec le texte voulu
        
        Arguments:
            text {string} -- texte que le template va contenir
            template {strin} -- nom du template
        """
        self.select_template(template)
        size = len(text)
        n1 = size%256   # calcule du reste de 256 voir doc brother
        n2 = size//256  # double / comme ça on ignore le reste de la division
        self.tcp_send(str.encode('^DI'+chr(n1)+chr(n2)+text,encoding='windows-1252'))


    def print_label(self):
        """envoie à l'imprimante l'ordre d'imprimer
        """
        self.tcp_send(str.encode("^QS1^FF")) # QS1 --> qualite priorite sur la qualite TS001 template 1 FF imprimer
