'''
Filename: h:\nicolas\fpv_soft_python\remote-imageFPV.py
Path: h:\nicolas\fpv_soft_python
Created Date: Thursday, April 30th 2020, 9:20:01 pm
Author: Nicolas  Sonnard

Copyright (c) 2020 Your Company
'''

import sys
from PyQt5.QtGui import *
from PyQt5.QtWidgets import *


from PyQt5.QtCore import pyqtSlot,pyqtSignal
from config_parser_init import*


from PyQt5.uic import loadUi
import configparser



class GUII(QDialog):
    def __init__(self):
        super(GUII,self).__init__() 
        loadUi('cofig_tool_GUI.ui',self)
        self.setWindowTitle("Config user")
        self.config = configparser.ConfigParser()
        self.pushButton.clicked.connect(self.save)
        self.load_config()

    def load_config(self):
        self.config.read('./config/config.cfg')
        self.lineEdit.setText(self.config.get('brother', 'ip'))
        self.lineEdit_2.setText(self.config.get('brother', 'port'))
        self.lineEdit_3.setText(self.config.get('cab_labels', 'ip'))
        self.lineEdit_4.setText(self.config.get('user_settings', 'name'))
        
    

    def save (self):
        print("save settings")
        self.config.set('brother','ip', str(self.lineEdit.text()))
        self.config.set('brother', 'port', str(self.lineEdit_2.text()))
        self.config.set('cab_labels', 'ip', str(self.lineEdit_3.text()))
        self.config.set('user_settings', 'name', str(self.lineEdit_4.text()))
        self.config.write(open('./config/config.cfg','w'))
        self.load_config()

   
            


app=QApplication(sys.argv)
widget=GUII()
widget.show()
sys.exit(app.exec())