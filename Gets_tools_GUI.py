# -*- coding: utf-8 -*-
"""
@author: Sonnardni
-V4919 correction bug dans thread de programmation.
    Quand un numero d'art n'était pas dans la base de donnée pas de stop du thread. - Maintenant il affiche l'erreur à l'utilisateur et stop le thread
-v5019
    Correction bug thread test maintenant des while attente le retour de l'arduino cela rend le trafique controlé
    ajout de la lecture de version soft et affiche dans le GUI l'infos 
    demande le numero de série à l'électronique et l'affiche dans le GUI
    si l'arduino ignore un data celui-ci est renvoyé 10x --> tempo entre les renvoie ajustée car beaucoup trop rapide avant
    Modification de la lecture d'un code barre, maintenant il affiche l'historique d'un produit
    Sécurtité améliorée lors d'une demande au server d'infos code bar --> si le serveur envoie une erreur on avertit l'utilisateur.
    correction d'un bug sur le numero d'article lors de la réimpression des étiquettes
    correction bug ajout au stock
    correction bug thread fin de test, le soft attendait un data du banc de test mais celui-ci allait pas en envoyer
    optimisation de la lecture du courant
    ajout de la version de soft dans la base de données
    enregistre les log de la console dans un fichier log sur le pc de la personne

-v0320 

    modification de l'impression des étiquettes tests et correction de la compatibilité entre les article générique et GEC
    correction de bug pour l'autocomplétation du champ soft
    ajout d'un champs initials pour le mariage d'un produit afin de garantir une meilleur traçabilité
-v0520

    correction erreur et ajout d'une sécurité si un chemin n'est pas configuré ou faux sur l'interface web on le signal ou on demande de l'entré manuellement si le payload du JSON ne contient pas
    la valeur SofwarePath
- v1120

    le banc test recevra les test et les différentes tâches afin de rendre le GUI plus simple
    - la liste des test sera demandée à google ensuite elle sera envoyée au banc test au format JSON
    - le banc tests fera les testes jusqu'à la fin ou au moment d'un éventuel problème.
    exemple de la structur JSON 
        {"typemsg": "tests","google": {"Boutons": true,"P4": true,"OPTO2-7": true,"Bouton_prise": true}}
        test

"""
import sys
import re
#sys.path.insert(0, "..\pythonNS_library")
from PyQt5.QtWidgets import QPushButton

from barcode import*
from PyQt5.QtWidgets import *
from PyQt5.QtGui import *
from PyQt5.QtCore import pyqtSlot,pyqtSignal, Qt,QSize
from PyQt5.uic import loadUi
from google_json import *
from gets_tools import *
from threading import Thread, Event
from Serial_Nicolas import*
from signaux_GUI import*
from printer import*
from config_parser_init import*
import asyncio
import datetime
import time
date = datetime.datetime.now()
import configparser
from queue import Queue
from mails import*



def VersionSoftCalculator():
    annee=int((time.strftime("%Y",time.gmtime(os.path.getmtime(r'./config/config.cfg')))))
    mois=int((time.strftime("%m",time.gmtime(os.path.getmtime(r'./config/config.cfg')))))
    jour=int((time.strftime("%d",time.gmtime(os.path.getmtime(r'./config/config.cfg')))))

    no_semaine=datetime.date(annee,mois,jour).isocalendar()[1]
    annee=str(annee)
    if int(no_semaine)<10:
        return "V0"+str(no_semaine)+annee[2]+annee[3]
    return "V"+str(no_semaine)+annee[2]+annee[3]



class open_ticket(QDialog):
    def __init__(self):
        super(open_ticket,self).__init__() 
        loadUi(r'./gui/open_ticket.ui',self)
        self.setWindowTitle("Signaler une erreur")
        self.config = configparser.ConfigParser()
        self.pushButton_2.clicked.connect(self.send_mail)
        self.load_config()
        self.mails=mails()

    def load_config(self):
        self.config.read(r'./config/config.cfg')
        self.lineEdit.setText(self.config.get('user_settings', 'name'))

    def send_mail(self):
        subject_mail=self.config.get('user_settings', 'name')+" demande du support"
        content_mail=str(self.textEdit.toPlainText())
        if (len(content_mail)<10):
           
            QMessageBox.information( self,"Infos", "Texte minimum 10 caractères")  
            return
            
        
        print (content_mail)
        self.mails.send_a_mail("ticket@gets-support.ch",subject_mail,content_mail)
        QMessageBox.information( self,"Infos", "Ticket ouvert il sera traité prochainement")  
        self.close() 


class CONFIG(QDialog):
    def __init__(self):
        super(CONFIG,self).__init__() 
        loadUi(r'./gui/config_tool_GUI.ui',self)
        self.setWindowTitle("Config user")
        self.config = configparser.ConfigParser()
        self.pushButton.clicked.connect(self.save)
        self.load_config()
        

    def load_config(self):
        self.config.read(r'./config/config.cfg')
        self.lineEdit.setText(self.config.get('brother', 'ip'))
        self.lineEdit_2.setText(self.config.get('brother', 'port'))
        self.lineEdit_3.setText(self.config.get('cab_labels', 'ip'))
        self.lineEdit_4.setText(self.config.get('user_settings', 'name'))
        self.lineEdit_5.setText(self.config.get('ti', 'path_hub'))
        self.lineEdit_6.setText(self.config.get('ti', 'path_gate_pager'))
        self.lineEdit_7.setText(self.config.get('ti', 'path_gateway'))
        
    

    def save (self):
        print("save")
        self.config.set('brother','ip', str(self.lineEdit.text()))
        self.config.set('brother', 'port', str(self.lineEdit_2.text()))
        self.config.set('cab_labels', 'ip', str(self.lineEdit_3.text()))
        self.config.set('user_settings', 'name', str(self.lineEdit_4.text()))
        self.config.set('ti', 'path_hub', str(self.lineEdit_5.text()))
        self.config.set('ti', 'path_gate_pager', str(self.lineEdit_6.text()))
        self.config.set('ti', 'path_gateway', str(self.lineEdit_7.text()))
        self.config.write(open(r'./config/config.cfg','w'))
        self.load_config()

    


class GUII(QMainWindow):
    def __init__(self):
        super(GUII,self).__init__() 
        loadUi(r'./gui/GUI.ui',self)
        self.queue_gets_tools=Queue(100)
        self.queue_signaux_gui=Queue(100)
        self.google_json=google_tools()
        self.run=True
        self.barcode_reader=barcode_reader()
        self.signaux_gui=signaux_gui(self)
        self.printer=printer()
        self.controller=controller()
        self.rx_datas_queue=Queue(100)
        self.tx_datas_queue=Queue(100)
        self.gets_tools=gets_tools(self.queue_gets_tools,self.rx_datas_queue,self.tx_datas_queue,self.queue_signaux_gui,self.signaux_gui)
 
        
        #self.tx_datas_queue.put('''{'productNumber': '110064', 'tests': {'Can_principal': [], 'Can_chambre': [], 'Boutons': []}}''',1)  
        



        #init du thread serial
        self.serial_thread=Thread(target = self.controller.serial, args=(self.rx_datas_queue,self.tx_datas_queue,self.queue_gets_tools,),daemon = True)

        #☺init thread signaux_gui
        self.signaux_gui_thread=Thread(target = self.signaux_gui.signaux, args=(self.queue_signaux_gui, ),daemon = True)


        # init du thread principal
        #self.main_thread=Thread(target = self.gets_tools.main_thread, args=(self.queue_gets_tools,self.run,self.rx_datas_queue,self.tx_datas_queue,self.queue_signaux_gui,self.signaux_gui, ))
        self.main_thread=Thread(target = self.gets_tools.main_thread,daemon = True)
        
        


        # start des differents thread
        self.signaux_gui_thread.start()
        self.serial_thread.start()
        self.main_thread.start()
        #lancement du gui

        #self.setWindowTitle("Gec tester "+VersionSoftCalculator())
        #self.showFullScreen()
        ## init des diférent boutons du gui
        self.pushButton.clicked.connect(self.start)

        self.actionAjout_d_articles.triggered.connect(self.config_user)
        #self.pushButton_3.clicked.connect(self.Connect_BT)
        #self.pushButton_5.clicked.connect(self.Clearlabel)
        #self.pushButton_4.clicked.connect(self.TestHard)


        self.pushButton_6.clicked.connect(self.imprime_labels)
        self.pushButton_7.clicked.connect(self.GetInfos)
        self.pushButton_11.clicked.connect(self.print_carton)
        self.pushButton_19.clicked.connect(self.clear_debug)
        self.pushButton_2.clicked.connect(self.prog_hub)
        self.pushButton_3.clicked.connect(self.prog_gateway)
        self.pushButton_13.clicked.connect(self.prog_gate_pager)
        self.pushButton_20.clicked.connect(self.open_ticket)
        self.signaux_gui.courant.connect(self.courant)
        self.signaux_gui.no_serie.connect(self.no_serie)
        self.signaux_gui.mac.connect(self.mac)
        self.signaux_gui.soft.connect(self.soft)
        self.signaux_gui.cpu.connect(self.cpu)
        self.signaux_gui.volts.connect(self.volts)
        self.signaux_gui.ok_box.connect(self.ok_box)
        self.signaux_gui.warning_box.connect(self.warning_box)
        self.signaux_gui.terminal_debug.connect(self.terminal_debug)
        self.signaux_gui.infos_box.connect(self.infos_box)
        self.signaux_gui.terminal_test.connect(self.terminal_tests)
        self.signaux_gui.terminal_infos_produits.connect(self.infos_produits_terminal)
        self.signaux_gui.get_serie.connect(self.get_serie_box)
        self.signaux_gui.clear_terminal.connect(self.clear_terminal)
        self.signaux_gui.soft.connect(self.soft)
        self.signaux_gui.label_name_soft.connect(self.label_name_soft_gui)

        self.signaux_gui.terminal_can_chambre.connect(self.terminal_can_chambre)
        self.signaux_gui.terminal_can_principal.connect(self.terminal_can_principal)
        self.signaux_gui.terminal_st.connect(self.terminal_st)
        
        self.signaux_gui.ti_stat.connect(self.ti_stat)
        self.signaux_gui.terminal_programation_ti.connect(self.terminal_programation_ti)
        
        self.signaux_gui.bt_statut.connect(self.bt_statut)
        
        #self.signaux_gui.msg_box_error.connect(self.msg_box_error)
        #self.signaux_gui.msg_box_infos.connect(self.msg_box_infos)
        self.pushButton_8.clicked.connect(self.startProgramming)
        self.pushButton_10.clicked.connect(self.Mariage)
        self.pushButton_15.clicked.connect(self.send_serie)
        self.pushButton_16.clicked.connect(self.send_mac)
        self.pushButton_14.clicked.connect(self.print_brut)
        # self.pushButton.connect(self.test)  # Connect your signals/slots
        #self.pushButton_9.clicked.connect(self.Connect_BT)
        #self.pushButton.setEnabled(False)
        #self.pushButton_2.setEnabled(False)
        #self.pushButton_4.setEnabled(False)
        # on desactive cette fonction car parfois un banc de tests n'est pas connecté pour la programmation
        #self.pushButton_8.setEnabled(False)
        self.config = configparser.ConfigParser()
        self.config.read(r'./config/config.cfg')
        self.lineEdit_7.setText(self.config.get('user_settings', 'name'))
        self.lineEdit_12.setText(self.config.get('user_settings', 'name'))
        self.lineEdit.setText(self.config.get('user_settings', 'name'))
        self.config.set('gets_tools', 'version_soft', VersionSoftCalculator())
        self.config.write(open(r'./config/config.cfg','w'))
        self.signaux_gui.etiquettes_cartons.connect(self.etiquettes_cartons_gui)

    def send_serie(self):

        Serie=self.lineEdit_16.text()

        if len(Serie)<10 or Serie[3]!="0":
            QMessageBox.warning(self, 'Erreur'," Sorry ! Entrer un numero de série correcte !! :-(")

        Serie=self.barcode_reader.number_barcode(Serie)

        print (Serie)
        NoSerie=int(Serie)
        SerieSimple=hex(NoSerie)[2:]

        

        buttonReply = QtWidgets.QMessageBox.question(self, 'Info numéro série', "le numero de série "+str(Serie)+"\r\n va être envoyé, voulez-vous continuer ??", QMessageBox.Yes | QMessageBox.No, QMessageBox.No)
        if buttonReply == QMessageBox.Yes:
            numero=SerieSimple[6]+SerieSimple[7]+SerieSimple[4]+SerieSimple[5]+SerieSimple[2]+SerieSimple[3]+SerieSimple[0]+SerieSimple[1]
            json_serie={"typemsg":"communication_bt","no_serie":numero}
            self.tx_datas_queue.put(json.dumps(json_serie),1)  

    def send_mac(self):
        mac=self.lineEdit_17.text()

        if len(mac)<10:
            QMessageBox.warning(self, 'Erreur'," Sorry ! Entrer un numero de série correcte !! :-(")
            return


        mac=self.barcode_reader.number_barcode(mac)

        mac=int(mac)
        mac=hex(mac)[2:]
        mac="00"+mac



        json_serie={"typemsg":"communication_bt","mac_adresse":mac}
        buttonReply = QtWidgets.QMessageBox.question(self, 'Info Mac', "La mac adresse "+str(mac)+"\r\n va être envoyé, voulez-vous continuer ??", QMessageBox.Yes | QMessageBox.No, QMessageBox.No)
        if buttonReply == QMessageBox.Yes:
            self.tx_datas_queue.put(json.dumps(json_serie),1)  

    def print_brut(self):
        self.printer.PrintEtiquette(self.lineEdit_15.text(),self.lineEdit_14.text(),self.lineEdit_22.text(),str(self.spinBox_3.value()),"Etiquette_prod.lbl")



    def terminal_can_chambre(self, txt):
        self.listWidget_8.addItem(txt)
        self.listWidget_8.scrollToBottom()

    def terminal_can_principal(self, txt):
        self.listWidget_7.addItem(txt)
        self.listWidget_7.scrollToBottom()





    def label_name_soft_gui(self, soft):
        self.label_30.setText(soft)
        if "NAK" in soft:
            self.label_30.setStyleSheet('color: red')
        else:
            self.label_30.setStyleSheet('color: green')



    def terminal_st(self, txt):
        self.listWidget_4.addItem(txt)
        self.listWidget_4.scrollToBottom()
        if "NAK"in txt:
            QMessageBox.warning(self, 'Erreur',txt)


    def startProgramming(self):
        self.listWidget_4.clear()
        if len (self.lineEdit_4.text())>5:    
            json_serie={"typemsg":"st_programming","numero":str(self.lineEdit_4.text())}
            print (json_serie)
            self.queue_gets_tools.put(json.dumps(json_serie))
               


    def ok_box(self,txt):
        QMessageBox.information(self, 'OK',txt)


    def warning_box(self,txt):
        QMessageBox.warning(self, 'Erreur',txt)
 
 
    def clear_debug(self):
        self.listWidget_11.clear()

    def clear_terminal(self):
        self.listWidget.clear()
        

    def terminal_debug(self,json_txt):
        if self.radioButton.isChecked():
            self.listWidget_11.addItem(json_txt)
            self.listWidget_11.scrollToBottom()      



    def prog_hub(self):
        json_ti_hub={}
        json_ti_hub ["typemsg"]="ti_programming"
        json_ti_hub ["path"]=self.config.get('ti', 'path_hub')
        # test si la case mac adresse est remplie et test la longueur du len 
        if len (self.lineEdit_8.text())>13:             
            json_ti_hub ["mac"]=self.lineEdit_8.text()

        self.queue_gets_tools.put(json.dumps(json_ti_hub))

    def prog_gateway(self):
        json_ti_gateway={}
        json_ti_gateway ["typemsg"]="ti_programming"
        json_ti_gateway ["path"]=self.config.get('ti', 'path_gateway')
        if len (self.lineEdit_8.text())>13:             
            json_ti_gateway ["mac"]=self.lineEdit_8.text()
        self.queue_gets_tools.put(json.dumps(json_ti_gateway))


    def prog_gate_pager(self):
        prog_gate_pager={}
        prog_gate_pager ["typemsg"]="ti_programming"
        prog_gate_pager ["path"]=self.config.get('ti', 'path_gate_pager')
        if len (self.lineEdit_8.text())>13:             
            prog_gate_pager ["mac"]=self.lineEdit_8.text()
        self.queue_gets_tools.put(json.dumps(prog_gate_pager))


    def ti_stat(self,datas):
        self.label_44.setText(datas)
        if "KO" in datas:
            self.label_44.setStyleSheet('color: red')
        if "OK" in datas:
            self.label_44.setStyleSheet('color: green')


    def cpu(self,datas):
        self.label_42.setText ("CPU\t"+str(datas)+"%")
        if float(datas)>70:
            self.label_42.setStyleSheet('color: orange')
        if float(datas)>90:
            self.label_42.setStyleSheet('color: red')
        if float(datas)<70:
            self.label_42.setStyleSheet('color: green')

    def infos_box(self,datas):
        self.label_43.setText (str(datas))
        self.label_43.setStyleSheet('color: red')
        if "OK" in datas:
            self.label_43.setText (str(datas))
            self.label_43.setStyleSheet('color: green')


    def soft(self,datas):
        self.label_12.setText (str(datas))
        self.label_64.setText (str(datas))
        self.label_12.setStyleSheet('color: pink')

    def volts(self,datas):
        self.label_8.setText (str(datas))

    def no_serie(self,datas):
        self.label_10.setText (str(datas))
        self.label_61.setText (str(datas))

    def mac(self,datas):
        self.label_14.setText (str(datas))
        self.label_62.setText (str(datas))

    def courant(self,datas):
        self.label_7.setText (str(datas))
        self.label_7.setStyleSheet('color: green')
        self.label_66.setText (str(datas))
        self.label_7.setStyleSheet('color: green')
    
    def bt_statut(self,datas):
        self.label_24.setText (str(datas))
        if "Connecté" in datas:
            self.label_24.setStyleSheet('color: green')
            self.pushButton.setEnabled(True)
        else:
            self.label_24.setStyleSheet('color: red')
            self.pushButton.setEnabled(False)
            self.label_7.setText    ("-")
            self.label_8.setText    ("-")
            self.label_10.setText   ("-")
            self.label_14.setText   ("-")
            self.label_12.setText   ("-")
            self.label_43.setText    (" ")
            self.listWidget.clear()




    def etiquettes_cartons_gui (self,boxed_quantity,request_quantity,infos_produits):
        self.label_34.setText ("Quantité total \r\n\t\t\t\t\t\t "+str(request_quantity))
        self.label_35.setText ("Quantité demandée \r\n\t\t\t\t\t\t "+str(request_quantity))
        self.listWidget_6.addItem(infos_produits)
        self.listWidget_6.scrollToBottom()

 


    def config_user(self):
        print("config")
        self.dialog=CONFIG()
        self.dialog.show()
        
    def open_ticket(self):
        print("config")
        self.dialog_ticket=open_ticket()
        self.dialog_ticket.show()


    def msg_box_error(self,txt):
        QMessageBox.information( self,"Infos", txt)  

    def msg_box_infos(self,txt):
        QMessageBox.critical( self,"ERROR", txt)  

    def imprime_labels(self):
        bv=self.barcode_reader.number_barcode(self.lineEdit_6.text())
        no6chfr=self.barcode_reader.number_barcode(self.lineEdit_9.text())
        quantity=str(self.spinBox.value())
        soft=""
        commentaire= self.plainTextEdit.toPlainText()
        initials=self.lineEdit_7.text()
        asyncio.run(self.printer.imprimerAvecBv(bv,no6chfr,quantity,soft,commentaire,initials,self.queue_signaux_gui))
   
    def terminal_programation_ti(self,txt):
        self.listWidget_10.addItem(txt)
        self.listWidget_10.scrollToBottom()                

    def terminal_tests(self,txt):
        self.listWidget.addItem(txt)
        self.listWidget.scrollToBottom()


    def infos_produits_terminal(self,txt):
        self.listWidget_5.addItem(txt)
        self.listWidget_5.scrollToBottom()
        self.lineEdit_4.clear()

    def print_carton(self): 
        #self.queue_gets_tools.put('''{"typmsg": "GUI","terminal":"tests","txt":" text a afficher"}''' )
        if len (self.lineEdit_12.text())<2:
            QMessageBox.critical( self,"ERROR", "Pas d'initials inscritent") 
            return
        if len (self.lineEdit_13.text())<5:
            QMessageBox.critical( self,"ERROR", "Pas de numéro de bv inscrit") 
            return
        if  self.spinBox_2.value()<1:
            QMessageBox.critical( self,"ERROR", "Quantité à 0") 
            return
            

        json_carton={}
        json_carton ["typemsg"]="carton_labels"
        json_carton ["datas"]={
            "initials":str(self.lineEdit_12.text()),
            "bv":str(self.barcode_reader.number_barcode(self.lineEdit_13.text())),
            "qty":str(self.spinBox_2.value())
        }    
        self.queue_gets_tools.put(json.dumps(json_carton))
        self.lineEdit_13.clear()

        

    
    def start(self):
        self.label_10.clear()
        if len (self.lineEdit.text())<2:
            QMessageBox.critical( self,"ERROR", "Pas d'initials inscritent") 
            return
        if len (self.lineEdit_2.text())<5:
            QMessageBox.critical( self,"ERROR", "Pas de numéro de bv inscrit") 
            return

        #self.tx_datas_queue.put('''{'productNumber': '110064', 'tests': {'Can_principal': [], 'Can_chambre': [], 'Boutons': []}}''',1)  

        json_start={}
        json_start ["typemsg"]="start_tests"
        json_start ["datas"]={
            "initials":str(self.lineEdit.text()),
            "bv":str(self.barcode_reader.number_barcode(str(self.lineEdit_2.text())))
        }    
        self.queue_gets_tools.put(json.dumps(json_start))
    
        

    def GetInfos(self):
        
        if len (self.lineEdit_3.text())<2:
            QMessageBox.critical( self,"ERROR", "Pas de code-barre scanné") 
            return

        json_getinfos={}
        json_getinfos ["typemsg"]="get_infos"
        json_getinfos ["datas"]={
            "code-barre":str(self.barcode_reader.number_barcode(self.lineEdit_3.text()))
        }    
        self.queue_gets_tools.put(json.dumps(json_getinfos))

    def Mariage(self):

        if len (self.lineEdit_11.text())<2:
            QMessageBox.critical( self,"ERROR", "Pas d'initial'") 
            return

        if len (self.lineEdit_10.text())<5:
            QMessageBox.critical( self,"ERROR", "Pas de bv scanné'") 
            return


        if len (self.lineEdit_5.text())<5:
            QMessageBox.critical( self,"ERROR", "Pas de produit scanné'") 
            return

        json_getinfos={}
        json_getinfos ["typemsg"]="mariage_product"
        json_getinfos ["datas"]={
            "initials":str(self.lineEdit_11.text()),
            "bv":str(self.barcode_reader.number_barcode(self.lineEdit_10.text())),
            "no_serie":str(self.lineEdit_5.text())
        }    
        self.queue_gets_tools.put(json.dumps(json_getinfos))

    def get_serie_box(self,SerieSimple):

        numero=SerieSimple[6]+SerieSimple[7]+SerieSimple[4]+SerieSimple[5]+SerieSimple[2]+SerieSimple[3]+SerieSimple[0]+SerieSimple[1]
        print (numero)
        numero = int(numero,16)
        #SerieSimple=hex(numero)[2:]
        print(SerieSimple)

        if "FFFFFFFF" in SerieSimple: 


            text, okPressed = QInputDialog.getText(self, "Get text","BRAVO :-) \r \n Les tests sont réussis !!!  \r \n Il faut maintenant coller l'étiquette et la scaner pour envoyer \r \n le numéro de série à l'électronique.", QLineEdit.Normal, "")
        
            if okPressed and text != '':

                if len(text)<10 or text[3]!="0": 
                    QMessageBox.warning(self, 'Erreur'," Sorry ! Entrer un numero de série SVP !! :-(")
                    return
                
                numero=SerieSimple[6]+SerieSimple[7]+SerieSimple[4]+SerieSimple[5]+SerieSimple[2]+SerieSimple[3]+SerieSimple[0]+SerieSimple[1]
                json_serie={"typemsg":"communication_bt","no_serie":numero}
                self.tx_datas_queue.put(json.dumps(json_serie),1)  
                SerieSimple=numero
        self.label_10.setText(str(numero))



config=config()
app=QApplication(sys.argv)
widget=GUII()
widget.show()
sys.exit(app.exec())



