

'''
Filename: c:\pythonNS_library\signaux_GUI.py
Path: c:\pythonNS_library
Created Date: Thursday, April 30th 2020, 5:37:49 pm
Author: Nicolas.Sonnard

Copyright (c) 2020 Your Company
'''


from PyQt5 import QtCore, QtGui, QtWidgets
from PyQt5.QtCore import pyqtSlot,pyqtSignal, Qt,QSize
import json
from log import*
import psutil
from datetime import datetime


class signaux_gui(QtCore.QObject):

    # signaux pour envoyer au terminal du gui
    terminal_test=QtCore.pyqtSignal(str)
    terminal_programation=QtCore.pyqtSignal(str)
    terminal_infos_produits=QtCore.pyqtSignal(str)
    etiquettes_cartons=QtCore.pyqtSignal(str,str,str)
    courant=QtCore.pyqtSignal(str)
    no_serie=QtCore.pyqtSignal(str)
    mac=QtCore.pyqtSignal(str)
    soft=QtCore.pyqtSignal(str)
    volts=QtCore.pyqtSignal(str)
    bt_statut=QtCore.pyqtSignal(str)
    cpu=QtCore.pyqtSignal(str)
    ti_stat=QtCore.pyqtSignal(str)
    infos_box=QtCore.pyqtSignal(str)
    terminal_programation_ti=QtCore.pyqtSignal(str)
    terminal_debug=QtCore.pyqtSignal(str)
    warning_box=QtCore.pyqtSignal(str)
    ok_box=QtCore.pyqtSignal(str)
    get_serie=QtCore.pyqtSignal(str)
    mac_adresse=QtCore.pyqtSignal(str)
    clear_terminal=QtCore.pyqtSignal()
    soft=QtCore.pyqtSignal(str)
    label_name_soft=QtCore.pyqtSignal(str)
    terminal_can_chambre=QtCore.pyqtSignal(str)
    terminal_st=QtCore.pyqtSignal(str)
    terminal_can_principal=QtCore.pyqtSignal(str)


    
    def __init__(self,path):
         super().__init__()
         print ("signaux start")
         self.run=True
         self.log=log()

    @QtCore.pyqtSlot()
    def signaux(self,queue_signaux_gui):
        # json de la fonction
        #  {"typemsg": "signal_gui","signaux_gui":{"terminal_tests": "string à afficher dans le terminal","terminal_programation": "string à afficher dans le terminal","terminal_Code-barres": "string à afficher dans le terminal"},"tests": {"Boutons": true,"P4": true,"OPTO2-7": true,"Bouton_prise": true}}
        while (self.run==True):
            self.queue_json=queue_signaux_gui.get(True,None)
            json_obj = json.loads(self.queue_json)
            #print("queue_signal")
            #print (self.queue_json)
            try:
                
                if "stop" in json_obj["typemsg"]:
                    self.run=False

                # {"typemsg":"communication","no_serie":"?"}

                print(str(json_obj))    
                    


                # on test le json afin de voir si cela est un message pour le GUI
                if "GUI" in json_obj["typemsg"]:
                    #print ("signal GUI")
                    # envoie sur le terminal de tests
                    if "terminal" in json_obj: 
                        
                        if "txt" in json_obj['terminal']:                 
                            try:
                                self.terminal_test.emit(str(json_obj['terminal']['txt']))
                            except:
                                self.log.logging("Erreur du json \t\t"+self.queue_json,"gets_tools.py")

                        # efface les infos sur le terminal
                        if "clear" in json_obj['terminal']:
                            try:
                                self.clear_terminal.emit()
                            except:
                                self.log.logging("Erreur du json \t\t"+self.queue_json,"gets_tools.py")

                    # affiche la version de soft                                           
                    if "soft" in json_obj:
                        try:
                            self.soft.emit(json_obj['soft'])
                        except:
                            self.log.logging("Erreur du json \t\t"+self.queue_json,"gets_tools.py")                    




                    # evnoie sur le terminal de programation des produits

                    if "programmation" in json_obj:                       
                        try:
                            self.terminal_programation.emit(str(json_obj['terminal']['txt']))
                        except:
                            self.log.logging("Erreur du json \t\t"+self.queue_json,"signaux_GUI.py")

                    # evnoie sur le terminal d'information des produits

                    if "terminal_5" in json_obj:                      
                        try:
                            self.terminal_infos_produits.emit(str(json_obj['terminal_5']['txt']))
                        except:
                            self.log.logging("Erreur du json \t\t"+self.queue_json,"signaux_GUI.py")
                    # evnoie sur le terminal d'information des produits
                    if "carton_infos" in json_obj:                      
                        try:
                            self.etiquettes_cartons.emit(str(json_obj['carton_infos']['boxedQuantity']),str(json_obj['carton_infos']['requestedQuantity']),str(json_obj['carton_infos']['infos_product']))
                        except:
                            self.log.logging("Erreur du json \t\t"+self.queue_json,"signaux_GUI.py")
                    # affiche la consomation des électronique sur le banc de test
                    # {"typemsg":"communication_bt","current":"33.77"}
                    if "ma" in json_obj:                      
                        try:
                            self.ma.emit(str(json_obj['current']))
                        except:
                            self.log.logging("Erreur du json \t\t"+self.queue_json,"signaux_GUI.py")                         
                    # affcihe les volts sur le GUI
                    if "volts" in json_obj:                      
                        try:
                            self.volts.emit(str(json_obj['volts']))
                        except:
                            self.log.logging("Erreur du json \t\t"+self.queue_json,"signaux_GUI.py") 
                    # affcihe les volts sur le GUI
                    if "mac" in json_obj:                      
                        try:
                            self.mac.emit(str(json_obj['mac']))
                        except:
                            self.log.logging("Erreur du json \t\t"+self.queue_json,"signaux_GUI.py")   
                    # affcihe les volts sur le GUI
                    if "soft" in json_obj:                      
                        try:
                            self.soft.emit(str(json_obj['soft']))
                        except:
                            self.log.logging("Erreur du json \t\t"+self.queue_json,"signaux_GUI.py")
                    # affcihe les volts sur le GUI
                    if "courant" in json_obj:                      
                        try:
                            self.courant.emit(str(json_obj['courant']))
                        except:
                            self.log.logging("Erreur du json \t\t"+self.queue_json,"signaux_GUI.py") 
                    # affiche le numero de serie
                    if "no_serie" in json_obj:                      
                        try:
                            self.no_serie.emit(str(json_obj['no_serie']))
                        except:
                            self.log.logging("Erreur du json \t\t"+self.queue_json,"signaux_GUI.py")  

                    # affiche le numero de serie
                    if "bt_statut" in json_obj:                      
                        try:
                            self.bt_statut.emit(str(json_obj['bt_statut']))
                        except:
                            self.log.logging("Erreur du json \t\t"+self.queue_json,"signaux_GUI.py")                                          
                    # affiche le CPU utilisé
                    if "cpu" in json_obj:                      
                        try:
                            self.cpu.emit(str(json_obj['cpu']))
                        except:
                            self.log.logging("Erreur du json \t\t"+self.queue_json,"signaux_GUI.py")                                          
                    # affiche un message sur l'écran des test --> message voyant ex 230V débranché.
                    if "infos_box" in json_obj:
                        try:
                            self.infos_box.emit(str(json_obj['infos_box']))
                        except:
                            self.log.logging("Erreur du json \t\t"+self.queue_json,"signaux_GUI.py")   
                    # affiche un message sur l'écran des test --> message voyant ex 230V débranché.
                    if "ti_terminal" in json_obj:
                        try:
                            self.terminal_programation_ti.emit(str(json_obj['ti_terminal']))
                        except:
                            self.log.logging("Erreur du json \t\t"+self.queue_json,"signaux_GUI.py")   
                    if "ti_state" in json_obj:
                        try:
                            self.ti_stat.emit(str(json_obj['ti_state']))
                        except:
                            self.log.logging("Erreur du json \t\t"+self.queue_json,"signaux_GUI.py") 
                    if "warning_box" in json_obj:
                        try:
                            self.warning_box.emit(str(json_obj['warning_box']))
                        except:
                            self.log.logging("Erreur du json \t\t"+self.queue_json,"signaux_GUI.py") 
                   
                    if "ok_box" in json_obj:
                        try:
                            self.ok_box.emit(str(json_obj['ok_box']))
                        except:
                            self.log.logging("Erreur du json \t\t"+self.queue_json,"signaux_GUI.py") 

                    if "label_name_soft" in json_obj:
                        try:
                            self.label_name_soft.emit(str(json_obj['label_name_soft']))
                        except:
                            self.log.logging("Erreur du json \t\t"+self.queue_json,"signaux_GUI.py") 

                    if "can_principal" in json_obj:
                        #str(datetime.datetime.now())+"\t\t"+
                        datetim_obj = datetime.now()
                        txt_final=str(datetim_obj)+"\tCode\t"+str(json_obj['can_principal']['code'])+"\t datas ->\t"+str(json_obj['can_principal']['datas'])
                        try:
                            self.terminal_can_principal.emit(txt_final)
                        except:
                            self.log.logging("Erreur du json \t\t"+self.queue_json,"signaux_GUI.py") 

                    if "can_chambre" in json_obj:
                        datetim_obj = datetime.now()
                        txt_final=str(datetim_obj)+"\tCode\t"+str(json_obj['can_chambre']['code'])+"\t datas ->\t"+str(json_obj['can_chambre']['datas'])
                        try:
                            self.terminal_can_chambre.emit(txt_final)
                        except:
                            self.log.logging("Erreur du json \t\t"+self.queue_json,"signaux_GUI.py") 

                    if "terminal_st" in json_obj:
                        try:
                            self.terminal_st.emit(str(json_obj['terminal_st']))
                        except:
                            self.log.logging("Erreur du json \t\t"+self.queue_json,"signaux_GUI.py") 

       


                    #check si c'est une demande de numero de serie
                    if "no_serie" in json_obj: 
                        print("numero serie class signaux_GUI")
                        self.get_serie.emit(str(json_obj['no_serie']))
                    #check si c'est une adresse mac
                    if "mac_adrreesse" in json_obj: 
                        self.mac_adresse.emit(str(json_obj['mac_adresse']))    

                    # envoie des donnees en brut sur le terminal de debug


                    try:
                        self.terminal_debug.emit(str(json_obj))
                    except:
                        self.log.logging("Erreur du json \t\t"+self.queue_json,"signaux_GUI.py") 
                    

                    #if "terminal_5" in json_obj:    
                    
            except:
                print ("Error parsing json \t\t"+self.queue_json)
                self.log.logging("Error parsing \t\t"+self.queue_json,"signaux_GUI.py")


        