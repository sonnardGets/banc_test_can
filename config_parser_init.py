import configparser
import os



class config(object):


    def __init__(self):
        ## lecture de la config placee dans le fichier config.cfg

        # test si le repertoir existe sinon il le crée et préremplis un minimum
        self.config = configparser.ConfigParser()
        if not os.path.exists('./config'):
            print("dossier inexistant")
            os.makedirs('./config')



        print ("read config")
        if os.path.isfile('./config/config.cfg')==False:
            print("creation d'une config de base")
            self.config.add_section('brother')
            self.config.set('brother','ip', '192.168.2.45')
            self.config.set('brother', 'port', '9100')
            self.config.add_section('cab_labels')
            self.config.set('cab_labels', 'ip', '192.168.2.103')
            self.config.set('cab_labels', 'etiquette_simple', 'Etiquette_prod.lbl')
            self.config.set('cab_labels', 'etiquette_ip', 'Etiquette_prod_IP.lbl')
            self.config.add_section('user_settings')
            self.config.set('user_settings', 'name', 'Nicolas Sonnard')
            self.config.add_section('gets_tools')
            self.config.set('gets_tools', 'version_soft', 'xxxxx')
            self.config.add_section('ti')
            self.config.set('ti', 'path_hub', '''\\192.168.2.8\Archives\archives\SMQ ISO9001\05 Programmation\Luminary\Programation R5301-RadioHub\DERNIER_SOFT''')
            self.config.set('ti', 'path_gate_pager', '''\\192.168.2.8\Archives\archives\SMQ ISO9001\05 Programmation\Luminary\Programmation 3000_Gate_Pager\Platine Generique PCB V2\DERNIER_SOFT''')
            self.config.set('ti', 'path_gateway', '''\\192.168.2.8\Archives\archives\SMQ ISO9001\05 Programmation\Luminary\Programation 3000-Gate-1\Platine Generique PCB V2\DERNIER_SOFT''')
            self.config.write(open('./config/config.cfg','w'))
    


