'''
Filename: c:\pythonNS_library\stm_programmer.py
Path: c:\pythonNS_library
Created Date: Thursday, April 30th 2020, 5:12:12 pm
Author: Nicolas.Sonnard

Copyright (c) 2020 Your Company
'''
import subprocess
import os
import sys
from signaux_GUI import*
import asyncio
import json

class stm_programmer (object):

    def __init__(self,gets_tools_queue):
        self.gets_tools_queue=gets_tools_queue
        


    def selec_file(self,path):
        """renvoie le nom d'un fichier contenu dans un répertoire ce répertoire doit contenir qu'un fichier

        Arguments:
            path {string} -- chemin du répertoir

        Returns:
            string -- nom du fichier contenu dans le repertoir
        """
        self.fichier=os.listdir(path)[0]
        print (self.fichier)

        return self.fichier

    def stm8(self,path):

        Commandeshell="cd stvp && STVP_CmdLine -BoardName=ST-LINK -Port=USB -ProgMode=SWIM \
        -Device=STM8S208C6 -verbose -no_loop -progress -verif \
        -FileProg="+'"'+path+"/"+self.selec_file(path)+'"'
            
        print (Commandeshell)


        proc=subprocess.Popen(Commandeshell, stdout=subprocess.PIPE, shell=True )
        
        for line in iter(proc.stdout.readline, b''):
            print(line.decode('utf-8'))
            test={"typemsg":"GUI","terminal_st":str(line.decode('utf-8'))}
            self.gets_tools_queue.put(json.dumps(test))
            if 'Verifying PROGRAM MEMORY succeeds' in str(line.decode('utf-8')):
                infos={"typemsg":"GUI","ok_box":"Programmation stm8 OK \r\n\r\n\r\nTout bon tout bon !"}
                self.gets_tools_queue.put(json.dumps(infos))

            if 'Erasing PROGRAM MEMORY fails' in str(line.decode('utf-8')):
                infos={"typemsg":"GUI","warning_box":"Quelque chose c'est mal passé !"}
                self.gets_tools_queue.put(json.dumps(infos))                 
            # do stuff with the line variable here
        proc.wait()

    def stm32(self,path):
  
        Commandeshell='''cd stvp && STVP_CmdLine -BoardName=ST-LINK -Port=USB -ProgMode=SWD \
            -Device=STM32F407xG -verbose -no_loop -progress -erase -blank \
            -verif -FileProg='''+'"'+path+"/"+self.selec_file(path)+'"'
        print (Commandeshell)
        #rocess = os.popen(Commandeshell)    

        proc=subprocess.Popen(Commandeshell, stdout=subprocess.PIPE, shell=True )
        
        for line in iter(proc.stdout.readline, b''):
            print(line.decode('utf-8'))
            test={"typemsg":"GUI","terminal_st":str(line.decode('utf-8'))}
            self.gets_tools_queue.put(json.dumps(test))
            if 'Verifying PROGRAM MEMORY succeeds' in str(line.decode('utf-8')):
                infos={"typemsg":"GUI","ok_box":"Programmation stm32 OK \r\n\r\n\r\nTout bon tout bon !"}
                self.gets_tools_queue.put(json.dumps(infos))

            if 'Erasing PROGRAM MEMORY fails' in str(line.decode('utf-8')):
                infos={"typemsg":"GUI","warning_box":"Quelque chose c'est mal passé !"}
                self.gets_tools_queue.put(json.dumps(infos))                 
            # do stuff with the line variable here
        proc.wait()
        

 
 

    async def start_prog(self,path):

        '''
        fonction permettant de lancer la proframmation des différents module 
        elle va regarder si un module contient un ou plusiers softs

        '''
       
        # convertion du format windows a un format correct
        if "\\" in path:
            path=path.replace("\\","/")

       



        if "STM8" in path:
            self.stm8(path)
            print ("programme stm8 soft -->  "+str(path))
            return "OK"
        else:
            if "STM32" in path:
                print ("programme stm32 soft -->  "+str(path))
                self.stm32(path)
                return "ok"
            else:
                print ("error path ")
                return "error soft"